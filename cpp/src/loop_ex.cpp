#include "loop.hpp"
#include <util.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>

#ifdef DATALOG
#define DATALOG_PROLOGUE                                \
    static int log_i = 0;                               \
    static bool time_set = false;                       \
    static std::chrono::steady_clock::time_point first; \
    if (!time_set) {                                    \
        time_set = true;                                \
        first = std::chrono::steady_clock::now();       \
    }
#else
#define DATALOG_PROLOGUE
#endif

#ifdef DATALOG
#define DATALOG_LOG(name, value)                         do { \
    if (log_i < DATALOG_SIZE) {                               \
        std::chrono::steady_clock::time_point now =           \
            std::chrono::steady_clock::now();                 \
        (name)[log_i].first = std::chrono::duration_cast      \
            <std::chrono::microseconds>(now - first).count(); \
        (name)[log_i].second = (value);                       \
        log_i++;                                              \
    }                                              } while(0)
#else
#define DATALOG_LOG(name, value)
#endif

#ifdef DATALOG
template<typename Log>
void printLog(Log *log, int size, std::ostream &ostream) {
    for (int i = 0; i < size; i++) {
        ostream << log[i].first;
        ostream << ',';
        ostream << log[i].second;
        ostream << std::endl;
    }
}
#endif

using namespace robofat::utility;


bool crusher::main_loop::wall_process(float dt) {
    DATALOG_PROLOGUE
    const int POLL_TIME = 25 * 1000;
    if (touch_sensor->is_pressed(false)) {
        messenger.pushMessage("- hit a wall");

        // go back (blocking call)
        control.travel(-mazeTouchTravelback, POLL_TIME, true, true);
        usleep(mazeTouchTravelbackWait);
        // turn (blocking call)
        control.turn(-mazeTouchTurnConstant, POLL_TIME, true, true);
        usleep(mazeTouchTurnWait);
        // reset wall PID
        pid_wall.get_state().reset();
        return true;
    } else {
        // get data
        float dist_org = us_sensor->distance_centimeters(false);
        DATALOG_LOG(distanceLog, (short) (dist_org * 10));
        float dist = std::min(dist_org, distanceClip);
        if (dist < freeThreshold) {
            // process data
            float distError = dist - distanceTarget;
            control.push(ftoi(pid_wall.process(distError, dt)));
            return false;
        } else {
            std::stringstream str;
            str << "- free side ";
            str << std::fixed << std::setw(5) << std::setprecision(1) << std::setfill(' ') << dist_org;
            messenger.pushMessage(str.str());
            control.travel(mazeFreeStraightDistance, POLL_TIME, true, false);
            control.arc(mazeFreeTurnDistance, mazeFreeTurnRatio, POLL_TIME);
            return true;
        }
    }
}

void crusher::main_loop::do_work() {
    run_flag.store(true);
    end_flag.store(false);

    control.push(0);
    arm.run();

    timespec lastWait;
    clock_gettime(CLOCK_MONOTONIC, &lastWait);
    timespec lastMeasure = lastWait; // copy

    // while the program should run
    while (run_flag.load()) {
        float diff = calc_delta_us(lastMeasure) / 1000.0f;
        if (diff <= 0)
            diff = 0.1; // 0.1 ms

        loop_phase state = tiltCycle.update();
        bool resetTime = regulators(diff, state);
        if (resetTime) {
            calc_delta_us(lastWait);
            lastMeasure = lastWait;
        }

        // wait to next iteration
        long deltatime = calc_delta_us(lastWait);
        long wait = sleep * 1000 - deltatime;
        if (wait > 0)
            usleep((useconds_t) wait);
    }
    control.stop();
    arm.stop();
#ifdef DATALOG
    std::ofstream    usFile("/tmp/us_log.csv");
    std::ofstream colorFile("/tmp/color_log.csv");
    printLog(distanceLog, DATALOG_SIZE,    usFile);
    printLog(   colorLog, DATALOG_SIZE, colorFile);
#endif
    end_flag.store(true);
}

bool crusher::main_loop::regulators(float diff, loop_phase state) {
    if (is_line(state)) {
        return line_process(diff);
    } else if (is_maze(state)) {
        return wall_process(diff);
    }
    return false;
}

///////////////////////////
// TUXOVA TARDIS FTW \o/ //
///////////////////////////

bool crusher::main_loop::line_process(float dt) {
    DATALOG_PROLOGUE

    int light = arm.getLight();
    int tacho = arm.getTacho();

#ifdef DATALOG
    std::pair<short, short> pair(tacho, light);
    DATALOG_LOG(colorLog, pair);
#endif

    int lightError = colorTarget - light;
    int sensorPower = doSensor(lightError, dt);
    int target = tacho + (int) roundf(sensorPower * targetCoefficient);
    int robot_ccw = calcRobot(target, dt);
    control.push(robot_ccw);

    return false;
}

int crusher::main_loop::doSensor(int lightError, float dt) {
    int power = (int) roundf(pid_sensor.process(lightError, dt));
    if (lm == line_mode::track_right)
        power *= -1;
    arm.setPower(power);
    return power;
}

int crusher::main_loop::calcRobot(int target, float dt) {
    int error = target - sensorCenter;
    int overflow = getSensorOverflow(target);
    if (overflow != 0) {
        error += overflow * overflowCoefficient;
    }
    float ccw = pid_wheels.process(error, dt);
    return (int) roundf(ccw);
}

int crusher::main_loop::getSensorOverflow(int destination) {
    int sensor_overflow = 0;
    if (destination > sensorMax) {
        sensor_overflow = destination - sensorMax;
    } else if (destination < sensorMin) {
        sensor_overflow = destination - sensorMin;
    }
    return sensor_overflow;
}

void crusher::main_loop::mazeToLine() {
    setLineParameters();
    pid_wheels.get_state().reset();
    pid_sensor.get_state().reset();
    arm.setPower(0);
    arm.run();
}

void crusher::main_loop::lineToMaze() {
    setMazeParameters();
    useconds_t WAIT = (useconds_t) config.get_integer("us motor prepare time");
    int power = config.get_integer("us motor prepare power");
    arm.alignAsync(lm, WAIT, power);
    pid_wall.get_state().reset();
}

void crusher::main_loop::setLineParameters() {
    control.enable_clip_overflow(config.get_bool("line overflow flow"));
    control.set_base_speed(config.get_integer(   "line speed base"));
    control.set_max_speed(config.get_integer(    "line speed max"));
    control.set_ramp_up(config.get_integer(      "line accelerate time"));
    control.set_ramp_down(config.get_integer(    "line decelerate time"));
}

void crusher::main_loop::setMazeParameters() {
    control.enable_clip_overflow(config.get_bool("maze overflow flow"));
    control.set_base_speed(config.get_integer(   "maze speed base"));
    control.set_max_speed(config.get_integer(    "maze speed max"));
    control.set_ramp_up(config.get_integer(      "maze accelerate time"));
    control.set_ramp_down(config.get_integer(    "maze decelerate time"));
}
