//
// Sensor arm implementation
//

#include "sensor_arm.hpp"
#include "ports.h"
#include <unistd.h>

using namespace robofat;

class aligner {
public:
    aligner(crusher::sensor_arm &arm, crusher::line_mode side, useconds_t wait, int power)
            : arm(arm), side(side), wait(wait), power(power) {}

    void align() {
        if (side == crusher::line_mode::track_left)
            arm.setPower(+power);
        else
            arm.setPower(-power);
        arm.run();
        usleep(wait);
        arm.stop();
    }

private:
    crusher::sensor_arm &arm;
    crusher::line_mode side;
    useconds_t wait;
    int power;
};

void *async_align(void *data);


crusher::sensor_arm::sensor_arm(ev3dev::address_type read, ev3dev::address_type write, display &scr) {
    color = get_device_heap_prompt<color_sensor>(read, scr, false, "color sensor");
    motor = get_device_heap_prompt<medium_motor>(write, scr, true, "sensor motor");
    color->set_mode(ev3dev::color_sensor::mode_col_reflect);
    motor->set_stop_action(ev3dev::large_motor::stop_action_brake);
}

int crusher::sensor_arm::getLight() {
    return color->reflected_light_intensity();
}

void crusher::sensor_arm::setPower(int power) {
    // clipping
    if (power < -100)
        power = 100;
    if (power > 100)
        power = 100;
    motor->set_duty_cycle_sp(power);
}

int crusher::sensor_arm::getTacho() {
    return motor->position();
}

void crusher::sensor_arm::resetTacho() {
    motor->reset();
}

void crusher::sensor_arm::stop() {
    motor->stop();
}

void crusher::sensor_arm::run() {
    motor->run_direct();
}

crusher::sensor_arm::~sensor_arm() {
    delete motor;
    delete color;
}

void crusher::sensor_arm::alignInit(line_mode lm, useconds_t wait, int power) {
    // align to the right side
    setPower(-power);
    run();
    usleep(wait);
    resetTacho();
    // go to the left side

    if (lm == line_mode::track_left) {
        setPower(power);
        run();
        usleep(wait);
        stop();
    }
}

void crusher::sensor_arm::alignAsync(line_mode lm, useconds_t wait, int power) {
    aligner *data = new aligner(*this, lm, wait, power);
    pthread_t thread;
    if (pthread_create(&thread, nullptr, async_align, (void *) data) != 0)
        throw std::runtime_error("Error creating thread.");
}

void *async_align(void *ptr) {
    aligner *data = (aligner *) ptr;
    data->align();
    delete data;
    pthread_exit(nullptr);
}
