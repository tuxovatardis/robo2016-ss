//
// Created by kuba on 9.11.16.
//

#ifndef CRUSHER_MESSAGE_QUEUE_HPP
#define CRUSHER_MESSAGE_QUEUE_HPP

#include <display.h>
#include <stdexcept>
#include <mutex>
#include <queue>

namespace crusher {
    class message_queue {
    public:
        message_queue(robofat::display &disp) : screen(disp) {}

        inline void printMessages() {
            msgMutex.lock();
            while (!msgQueue.empty()) {
                screen.print(msgQueue.front());
                msgQueue.pop();
            }
            msgMutex.unlock();
        }

        inline void pushMessage(std::string const &message) {
            msgMutex.lock();
            msgQueue.push(message);
            msgMutex.unlock();
        };

        inline std::string popMessage() {
            msgMutex.lock();
            if (msgQueue.empty())
                throw std::runtime_error("no more messages");
            std::string value = msgQueue.front();
            msgQueue.pop();
            msgMutex.unlock();
            return value;
        }

        inline bool hasMessages() {
            msgMutex.lock();
            bool value = msgQueue.empty();
            msgMutex.unlock();
            return value;
        }

    private:
        std::queue<std::string> msgQueue;
        std::mutex msgMutex;
        robofat::display &screen;
    };
}


#endif //CRUSHER_MESSAGE_QUEUE_HPP
