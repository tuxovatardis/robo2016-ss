//
// Created by kuba on 5.11.16.
//

#ifndef CRUSHER_MODE_HPP
#define CRUSHER_MODE_HPP

namespace crusher {
    enum class line_mode {
        track_left, track_right
    };
}

#endif //CRUSHER_MODE_HPP
