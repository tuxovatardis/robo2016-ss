#include "loop.hpp"
#include <ports.h>
#include <util.h>

using namespace robofat::utility;

crusher::main_loop::main_loop(robofat::settings &config,
                              robofat::display &disp,
                              line_mode line)
        : config(config),
          screen(disp),
          messenger(disp),
          lm(line),
          control(config.get_port("motor port left"), config.get_port("motor port right"), &disp),
          arm(config.get_port("color sensor port"), config.get_port("color motor port"), disp),
          tiltCycle(config.get_bool("maze start") ? loop_phase::maze_traversal : loop_phase::line_flat,
                    config.get_port("gyro sensor port"),
                    config.get_bool("tilt by rate") ? config.get_integer("tilt rate threshold")
                                                    : config.get_integer("tilt angle threshold"),
                    config.get_bool("tilt by rate") ? tilt_detection::rate
                                                    : tilt_detection::angle,
                    config.get_bool("tilt color"),
                    config.get_integer("tilt color threshold"),
                    disp,
                    *this),
          run_flag(true),
          end_flag(false),
          sensorMin(config.get_integer("color motor tacho min")),
          sensorMax(config.get_integer("color motor tacho max")),
          colorTarget(config.get_integer("color target")),
          sensorCenter(config.get_integer("color motor center")),
          targetCoefficient(config.get_integer("color target coefficient")),
          overflowCoefficient(config.get_integer("color overflow coefficient")),
          sleep(config.get_integer("sleep")),
          mazeTouchTravelbackWait((useconds_t) config.get_integer("touch travelback wait")),
          mazeTouchTurnWait((useconds_t) config.get_integer("touch turn wait")),

          distanceTarget(config.get_integer("wall target")),
          mazeTouchTravelback(config.get_integer("touch travelback")),
          mazeTouchTurnConstant(config.get_integer("touch turn")),
          distanceClip(config.get_float("us dist max")),
          freeThreshold(config.get_float("us dist limit")),
          mazeFreeTurnRatio(config.get_float("turn ratio")),
          mazeFreeStraightDistance(config.get_integer("turn dist straight")),
          mazeFreeTurnDistance(config.get_integer("turn dist curve")),
          pid_wheels(config.get_float("wheels kp"),
                     config.get_float("wheels ki"),
                     config.get_float("wheels kd"),
                     config.get_float("wheels kdf now"),
                     config.get_float("wheels kdf hist")),
          pid_sensor(config.get_float("color kp"),
                     config.get_float("color ki"),
                     config.get_float("color kd"),
                     config.get_float("color kdf now"),
                     config.get_float("color kdf hist")),
          pid_wall(config.get_float("wall kp"),
                   config.get_float("wall ki"),
                   config.get_float("wall kd"),
                   config.get_float("wall kdf now"),
                   config.get_float("wall kdf hist")) {
    if (config.get_bool("maze start")) {
        setMazeParameters();
    } else {
        setLineParameters();
    }
    control.set_left_invert(config.get_bool("motor invert left"));
    control.set_right_invert(config.get_bool("motor invert right"));

    us_sensor = robofat::get_device_heap_prompt<ev3dev::ultrasonic_sensor>(
            config.get_port("us sensor port"), disp, false, "US sensor");
    touch_sensor = robofat::get_device_heap_prompt<ev3dev::touch_sensor>(
            config.get_port("touch port"), disp, false, "Touch sensor");
    us_sensor->set_mode(ev3dev::ultrasonic_sensor::mode_us_dist_cm);

    touch_sensor->set_mode(ev3dev::touch_sensor::mode_touch);
    tiltCycle.color = arm.getSensor();
    tiltCycle.touch = touch_sensor;
#ifdef DATALOG
    colorLog = new std::pair<long, std::pair<short, short>>[DATALOG_SIZE];
 distanceLog = new std::pair<long, short>                  [DATALOG_SIZE];
#endif
}

void crusher::main_loop::prepare() {
    useconds_t WAIT = (useconds_t) config.get_integer("us motor prepare time");
    int power = config.get_integer("us motor prepare power");
    arm.alignInit(lm, WAIT, power);
}


crusher::main_loop::~main_loop() {
    delete touch_sensor;
    delete us_sensor;
#ifdef DATALOG
    delete[]    colorLog;
    delete[] distanceLog;
#endif
}