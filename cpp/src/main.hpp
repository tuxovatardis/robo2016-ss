//
// Created by kuba on 5.11.16.
//

#ifndef CRUSHER_MAIN_HPP
#define CRUSHER_MAIN_HPP

#include <settings.h>
#include <display.h>
#include <atomic>
#include "mode.hpp"
#include "loop.hpp"

extern pthread_t worker_thread;

extern void thread_start(crusher::main_loop &loop);

extern void *worker_fn(void *loop);

extern robofat::settings init_cfg(const char *file);

extern crusher::line_mode getline(robofat::display &disp);

extern void show_intro(robofat::display &lcd, crusher::line_mode line);

extern int main(int argc, char **argv);

#endif //CRUSHER_MAIN_HPP
