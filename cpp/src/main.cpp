#include "main.hpp"
#include <led.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>

#define PTHREAD_THROW(x) do { if((x) != 0) throw std::runtime_error("pthread err"); } while(0)

using robofat::settings;
using robofat::display;
using robofat::led_set;
using robofat::led_color;
using crusher::main_loop;
using crusher::line_mode;

const size_t PATH_LEN = 255;
const char CONF_FILE[PATH_LEN] = "/etc/crusher.conf";

pthread_t worker_thread;

settings init_cfg(const char *file) {
    settings cfg;
    {
        std::ifstream istr(file, std::ios::in);
        int error = cfg.load(istr, true, true);
        if (error) {
            std::cerr << "\a";
            std::cerr << "FATAL ERROR: CANNOT OPEN CONFIG FILE!" << std::endl;
            throw std::runtime_error("load error");
        }
    }
    return cfg;
}

int main(int argc, char **argv) {
    display lcd;

    led_set(led_color::AMBER, led_color::AMBER);
    line_mode line = getline(lcd);

    led_set(led_color::RED, led_color::RED);
    lcd.init_begin("CFG LOADER");
    settings cfg = init_cfg(argc > 1 ? argv[1] : CONF_FILE);
    lcd.init_end(true);

    led_set(led_color::ORANGE, led_color::ORANGE);
    lcd.init_begin("MAINLOOP INIT");
    main_loop loop(cfg, lcd, line);
    lcd.init_end(true);

    led_set(led_color::YELLOW, led_color::YELLOW);
    lcd.init_begin("SENSOR");
    loop.prepare();
    lcd.init_end(true);

    led_set(led_color::GREEN, led_color::GREEN);
    show_intro(lcd, line);

    lcd.settimeout(0); // nonblock
    {
        display::keys key;
        do {
            usleep(100 * 1000);
            key = lcd.getkey();
        } while (key != display::key_enter &&
                 key != display::key_escape);
        if (key == display::key_escape)
            return 0;
    }

    thread_start(loop);

    auto &msgr = loop.getMessenger();
    do {
        display::keys key = lcd.getkey();
        if (key == display::key_escape) {
            break;
        }
        msgr.printMessages();
        usleep(100 * 1000);
    } while (!loop.ended());
    loop.terminate();

    PTHREAD_THROW(pthread_join(worker_thread, nullptr));
    pthread_exit(nullptr);
}

void show_intro(robofat::display &lcd, crusher::line_mode line) {
    lcd.clear_display();

    lcd.print("Robot Crusher", display::attr_underline);
    lcd.print(" by Jakub Vanek");
    lcd.print("");
    lcd.print("");

    lcd.print_centered("START MAYHEM", display::attr_bold);
    std::stringstream str;
    str << "Line side: ";
    switch (line) {
        case line_mode::track_left:
            str << "Left";
            break;
        case line_mode::track_right:
            str << "Right";
            break;
    }
    lcd.print_centered(str.str());

}


crusher::line_mode getline(robofat::display &disp) {
    const std::string prompt = "LINE SIDE:";
    const std::vector<std::string> opt = {"^^ L ^^", "vv R vv"};
    int selected = disp.show_menu(prompt, opt, 0, false);
    switch (selected) {
        case 0:
            return crusher::line_mode::track_left;
        case 1:
            return crusher::line_mode::track_right;
        default:
            throw std::runtime_error("prompt error");
    }
}

void *worker_fn(void *ptr) {
    crusher::main_loop &loop = *(crusher::main_loop *) ptr;
    loop.do_work();
    pthread_exit(nullptr);
}

void thread_start(crusher::main_loop &loop) {
    pthread_attr_t attr;
    PTHREAD_THROW(pthread_attr_init(&attr));
    PTHREAD_THROW(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE));
    PTHREAD_THROW(pthread_create(&worker_thread, &attr, worker_fn, &loop));
}
