//
// Created by kuba on 9.11.16.
//

#ifndef CRUSHER_TILT_CYCLE_HPP
#define CRUSHER_TILT_CYCLE_HPP

#include <ev3dev.h>

#include <settings.h>
#include <display.h>

using namespace robofat;

namespace crusher {

    enum class loop_phase {
        line_flat,
        line_up,
        line_down,
        maze_traversal
    };

    inline bool is_line(loop_phase phase) {
        return phase == loop_phase::line_up || phase == loop_phase::line_down || phase == loop_phase::line_flat;
    }

    inline bool is_maze(loop_phase phase) {
        return phase == loop_phase::maze_traversal;
    }

    class main_loop;

    enum class tilt_detection {
        rate, angle
    };
    class tilt_cycle {
    public:
        tilt_cycle() = delete;

        tilt_cycle(ev3dev::address_type const &gyro_port,
                   int threshold,
                   tilt_detection detection,
                   bool color_trig,
                   int color_threshold,
                   display &screen,
                   main_loop &loop);

        tilt_cycle(loop_phase start,
                   ev3dev::address_type const &gyro_port,
                   int threshold,
                   tilt_detection detection,
                   bool color_trig,
                   int color_threshold,
                   display &screen,
                   main_loop &loop);

        tilt_cycle(tilt_cycle const &ref) = delete;

        tilt_cycle(tilt_cycle &&ref) = delete;

        ~tilt_cycle();

        tilt_cycle &operator=(tilt_cycle const &other) = delete;

        tilt_cycle &operator=(tilt_cycle &&other) = delete;

        loop_phase update();
        ev3dev::touch_sensor *touch;
        ev3dev::color_sensor *color;

    private:
        ev3dev::gyro_sensor *gyro;
        main_loop &loop;
        const bool rate;
        const bool color_trig;
        const int threshold;
        const int color_threshold;



        loop_phase actual;
        loop_phase update_rate();
        loop_phase update_angle();
    };
}


#endif //CRUSHER_TILT_CYCLE_HPP
