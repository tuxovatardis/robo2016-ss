//
// Sensor arm
//

#ifndef CRUSHER_ARM_H
#define CRUSHER_ARM_H

#include <ev3dev.h>
#include <display.h>
#include "mode.hpp"

using namespace robofat;

namespace crusher {
	/**
	 * \brief Sensor arm.
	 */
	class sensor_arm {
	public:
		/**
		 * \brief Construct new sensor arm.
		 *
		 * @param read    Port with light sensor connected.
		 * @param write   Port with medium motor connected.
		 * @param scr     Display
		 */
		sensor_arm(ev3dev::address_type read, ev3dev::address_type write, display &scr);

		/**
		 * \brief Deallocate sensors.
		 */
		~sensor_arm();

		/**
		 * \brief Get light value.
		 *
		 * @return Value in range 0-100 (%).
		 */
		int getLight();

		/**
		 * \brief Set sensor motor power.
		 *
		 * @param power Sensor power in range -100 to 100 (%).
		 */
		void setPower(int power);

		/**
		 * \brief Get sensor motor tacho.
		 *
		 * @return Sensor motor tacho in degrees.
		 */
		int getTacho();

		/**
		 * \brief Reset the sensor motor (and reset the tachometer)
		 */
		void resetTacho();

		/**
		 * \brief Stop the sensor motor.
		 */
		void stop();

		/**
		 * \brief Start the sensor motor movement.
		 */
		void run();

        void alignInit(line_mode lm, useconds_t wait, int power);
        void alignAsync(line_mode lm, useconds_t wait, int power);

		inline ev3dev::color_sensor *getSensor() {
			return color;
		}

		inline ev3dev::medium_motor *getMotor() {
			return motor;
		}
	private:
		ev3dev::color_sensor *color; ///< Color sensor for data input
		ev3dev::medium_motor *motor; ///< Medium motor for data output
	};
}
#endif //CRUSHER_ARM_H
